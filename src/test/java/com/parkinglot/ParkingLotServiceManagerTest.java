package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotServiceManagerTest {
    @Test
    void should_return_ticket_with_rightParkingLot_when_park_given_parkingLotServiceManager_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        //when
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager(parkingLot);
        ParkingTicket parkingTicket = parkingLotServiceManager.park(new Car());
        //then
        Assertions.assertNotNull(parkingTicket);
    }

    @Test
    void should_return_car_when_fetch_given_parkingLotServiceManager_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car = new Car();
        ParkingTicket ticket= parkingLot.park(car);
        //when
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager(parkingLot);
        Car fetchedCar = parkingLotServiceManager.fetch(ticket);
        //then
        Assertions.assertEquals(car,fetchedCar);
    }

    @Test
    void should_return_each_ticket_when_park_given_parkingLotServiceManager_and_two_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car1 = new Car();
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager(parkingLot);
        ParkingTicket ticket1 = parkingLotServiceManager.park(car1);

        Car car2 = new Car();
        ParkingTicket ticket2 = parkingLotServiceManager.park(car2);
        //when
        //then
        Assertions.assertNotEquals(ticket1,ticket2);
    }

    @Test
    void should_return_exception_when_fetch_given_parkingLotServiceManager_and_ticket_not_in_parkingLotServiceManager_parkinglot() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingLot parkingLot2= new ParkingLot(2);
        Car car = new Car();
        ParkingTicket ticket= parkingLot2.park(car);
        //when
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager(parkingLot);

        //then
        NotMyParkingLotException notMyParkingLotException = Assertions.assertThrows(NotMyParkingLotException.class, () -> parkingLotServiceManager.fetch(ticket));
        Assertions.assertEquals("Not MyParkingLot ",notMyParkingLotException.getMessage());
    }

    @Test
    void should_return_ticket_when_park_given_parkingLotServiceManager_and_car_and_parkingBoy() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        //when
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager(parkingLot,parkingBoy);
        ParkingTicket parkingTicket = parkingLotServiceManager.parkByParkingBoy(new Car(),parkingBoy);
        //then
        Assertions.assertNotNull(parkingTicket);
    }

    @Test
    void should_return_exception_when_park_given_parkingLotServiceManager_and_car_and_parkingBoy_withFullParkingLot() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        for (int i = 0; i < parkingLot.getCapacity(); i++) {
            parkingLot.park(new Car());
        }
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        //when
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager(parkingLot,parkingBoy);
        //then
        ParkingBoyCantCompleteException parkingBoyCantCompleteException = Assertions
                .assertThrows(ParkingBoyCantCompleteException.class, () -> parkingLotServiceManager.parkByParkingBoy(new Car(), parkingBoy));

        Assertions.assertEquals("ParkingBoy Cant Complete",parkingBoyCantCompleteException.getMessage());
    }

    @Test
    void should_return_car_when_fetch_given_parkingLotServiceManager_and_ticket_and_parkingBoy() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car = new Car();
        //when
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager(parkingLot,parkingBoy);
        ParkingTicket parkingTicket = parkingLotServiceManager.parkByParkingBoy(car,parkingBoy);
        Car car1 = parkingLotServiceManager.fetchByParkingBoy(parkingTicket,parkingBoy);
        //then
        Assertions.assertEquals(car,car1);
    }

    @Test
    void should_return_exception_when_fetch_given_parkingLotServiceManager_and_Usedticket_and_parkingBoy() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car = new Car();
        //when
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager(parkingLot,parkingBoy);
        ParkingTicket parkingTicket = parkingLotServiceManager.parkByParkingBoy(car,parkingBoy);
        parkingLotServiceManager.fetchByParkingBoy(parkingTicket,parkingBoy);
        //then
        ParkingBoyCantCompleteException parkingBoyCantCompleteException = Assertions
                .assertThrows(ParkingBoyCantCompleteException.class, () -> parkingLotServiceManager.fetchByParkingBoy(parkingTicket, parkingBoy));

        Assertions.assertEquals("ParkingBoy Cant Complete",parkingBoyCantCompleteException.getMessage());
    }

    @Test
    void should_return_exception_when_fetch_given_parkingLotServiceManager_and_Wrongticket_and_parkingBoy() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car = new Car();
        //when
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager(parkingLot,parkingBoy);
        ParkingTicket parkingTicket = parkingLotServiceManager.parkByParkingBoy(car,parkingBoy);
        parkingLotServiceManager.fetchByParkingBoy(parkingTicket,parkingBoy);
        //then
        ParkingBoyCantCompleteException parkingBoyCantCompleteException = Assertions
                .assertThrows(ParkingBoyCantCompleteException.class, () -> parkingLotServiceManager.fetchByParkingBoy(new ParkingTicket(), parkingBoy));

        Assertions.assertEquals("ParkingBoy Cant Complete",parkingBoyCantCompleteException.getMessage());
    }
}
