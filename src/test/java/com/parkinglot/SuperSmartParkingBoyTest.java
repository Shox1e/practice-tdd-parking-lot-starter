package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SuperSmartParkingBoyTest {
    @Test
    void should_return_ticket_with_rightParkingLot_when_park_given_superSmartParkingBoy_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);
        ParkingLot parkingLot3 = new ParkingLot(3);
        parkingLot.park(new Car());
        parkingLot.park(new Car());
        parkingLot2.park(new Car());
        //when
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot,parkingLot2,parkingLot3);
        ParkingTicket parkingTicket = superSmartParkingBoy.park(new Car());
        //then
        Assertions.assertEquals(3,parkingTicket.getNumber());
    }

    @Test
    void should_return_2car_when_fetch_given_superSmartParkingBoy_and_2ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        parkingLot.park(new Car());
        ParkingLot parkingLot2 = new ParkingLot(2);
        parkingLot2.park(new Car());
        ParkingLot parkingLot3 = new ParkingLot(3);
        parkingLot3.park(new Car());
        Car car = new Car();
        Car car1 = new Car();
        //when
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot,parkingLot2,parkingLot3);
        ParkingTicket parkingTicket = superSmartParkingBoy.park(car);
        ParkingTicket parkingTicket1 = superSmartParkingBoy.park(car1);
        Car fetchedCar = superSmartParkingBoy.fetch(parkingTicket);
        Car fetchedCar1 = superSmartParkingBoy.fetch(parkingTicket1);
        //then
        Assertions.assertEquals(car,fetchedCar);
        Assertions.assertEquals(car1,fetchedCar1);
    }

    @Test
    void should_return_each_ticket_when_park_given_superSmartParkingBoy_and_two_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);
        Car car1 = new Car();
        Car car2 = new Car();
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot,parkingLot2);
        ParkingTicket ticket1 = superSmartParkingBoy.park(car1);

        ParkingTicket ticket2 = superSmartParkingBoy.park(car2);
        //when
        //then
        Assertions.assertNotEquals(ticket1,ticket2);
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_superSmartParkingBoy_and_wrongticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car1 = new Car();
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot);
        ParkingTicket parkingTicket = superSmartParkingBoy.park(car1);
        //when
        superSmartParkingBoy.fetch(parkingTicket);
        //then
        UnRecognizedParkingTicketException unRecognizedParkingTicketException = Assertions.assertThrows(UnRecognizedParkingTicketException.class, () -> superSmartParkingBoy.fetch(new ParkingTicket()));
        Assertions.assertEquals("Unrecognized ParkingTicket",unRecognizedParkingTicketException.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_superSmartParkingBoy_and_Usedticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car1 = new Car();
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot);
        ParkingTicket ticket1= superSmartParkingBoy.park(car1);
        //when
        superSmartParkingBoy.fetch(ticket1);
        //then
        UnRecognizedParkingTicketException unRecognizedParkingTicketException = Assertions.assertThrows(UnRecognizedParkingTicketException.class, () -> superSmartParkingBoy.fetch(ticket1));
        Assertions.assertEquals("Unrecognized ParkingTicket",unRecognizedParkingTicketException.getMessage());
    }

    @Test
    void should_return_nothing_when_park_given_superSmartParkingBoy_without_any_postion() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        for (int i = 0; i < parkingLot.getCapacity(); i++) {
            parkingLot.park(new Car());
        }
        ParkingLot parkingLot1 = new ParkingLot(2);
        for (int i = 0; i < parkingLot1.getCapacity(); i++) {
            parkingLot1.park(new Car());
        }
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot,parkingLot1);
        //when
        NoAvailablePostionException noAvailablePostionException = Assertions.assertThrows(NoAvailablePostionException.class, () -> superSmartParkingBoy.park(new Car()));
        //then
        Assertions.assertEquals("No Available Postion",noAvailablePostionException.getMessage());
    }
}
