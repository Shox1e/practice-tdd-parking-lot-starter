package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {

    @Test
    void should_return_ticket_when_park_given_parkinglot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car = new Car();
        //when
        ParkingTicket ticket= parkingLot.park(car);
        //then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parkinglot_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car = new Car();
        ParkingTicket ticket= parkingLot.park(car);
        //when
        Car fetchedCar = parkingLot.fetch(ticket);
        //then
        Assertions.assertEquals(car,fetchedCar);
    }

    @Test
    void should_return_each_ticket_when_park_given_parkinglot_and_two_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car1 = new Car();
        ParkingTicket ticket1= parkingLot.park(car1);

        Car car2 = new Car();
        ParkingTicket ticket2= parkingLot.park(car2);
        //when
        //then
        Assertions.assertNotEquals(ticket1,ticket2);
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_parkinglot_and_wrongticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car1 = new Car();
        ParkingTicket ticket1= parkingLot.park(car1);
        //when
        parkingLot.fetch(ticket1);
        //then
        UnRecognizedParkingTicketException unRecognizedParkingTicketException = Assertions.assertThrows(UnRecognizedParkingTicketException.class, () -> parkingLot.fetch(new ParkingTicket()));
        Assertions.assertEquals("Unrecognized ParkingTicket",unRecognizedParkingTicketException.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_parkinglot_and_Usedticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car1 = new Car();
        ParkingTicket ticket1= parkingLot.park(car1);
        //when
        parkingLot.fetch(ticket1);
        //then
        UnRecognizedParkingTicketException unRecognizedParkingTicketException = Assertions.assertThrows(UnRecognizedParkingTicketException.class, () -> parkingLot.fetch(ticket1));
        Assertions.assertEquals("Unrecognized ParkingTicket",unRecognizedParkingTicketException.getMessage());
    }

    @Test
    void should_return_nothing_when_park_given_parkinglot_without_any_postion() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        parkingLot.park(new Car());
        parkingLot.park(new Car());
        parkingLot.park(new Car());
        parkingLot.park(new Car());
        parkingLot.park(new Car());
        parkingLot.park(new Car());
        parkingLot.park(new Car());
        parkingLot.park(new Car());
        parkingLot.park(new Car());
        parkingLot.park(new Car());
        //when
        NoAvailablePostionException noAvailablePostionException = Assertions.assertThrows(NoAvailablePostionException.class, () -> parkingLot.park(new Car()));
        //then
        Assertions.assertEquals("No Available Postion",noAvailablePostionException.getMessage());
    }
}
