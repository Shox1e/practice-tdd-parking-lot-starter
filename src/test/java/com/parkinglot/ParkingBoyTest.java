package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingBoyTest {

    @Test
    void should_return_ticket_when_park_given_parkingBoy_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car = new Car();
        //when
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        parkingBoy.park(car);
        ParkingTicket ticket= parkingLot.park(car);
        //then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parkingBoy_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car = new Car();
        ParkingTicket ticket= parkingLot.park(car);
        //when
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car fetchedCar = parkingBoy.fetch(ticket);
        //then
        Assertions.assertEquals(car,fetchedCar);
    }

    @Test
    void should_return_each_ticket_when_park_given_parkingBoy_and_two_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car1 = new Car();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        ParkingTicket ticket1 = parkingBoy.park(car1);

        Car car2 = new Car();
        ParkingTicket ticket2 = parkingBoy.park(car2);
        //when
        //then
        Assertions.assertNotEquals(ticket1,ticket2);
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_parkingBoy_and_wrongticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car1 = new Car();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        ParkingTicket parkingTicket = parkingBoy.park(car1);
        //when
        parkingBoy.fetch(parkingTicket);
        //then
        UnRecognizedParkingTicketException unRecognizedParkingTicketException = Assertions.assertThrows(UnRecognizedParkingTicketException.class, () -> parkingBoy.fetch(new ParkingTicket()));
        Assertions.assertEquals("Unrecognized ParkingTicket",unRecognizedParkingTicketException.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_parkingBoy_and_Usedticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car1 = new Car();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        ParkingTicket ticket1= parkingBoy.park(car1);
        //when
        parkingBoy.fetch(ticket1);
        //then
        UnRecognizedParkingTicketException unRecognizedParkingTicketException = Assertions.assertThrows(UnRecognizedParkingTicketException.class, () -> parkingBoy.fetch(ticket1));
        Assertions.assertEquals("Unrecognized ParkingTicket",unRecognizedParkingTicketException.getMessage());
    }

    @Test
    void should_return_nothing_when_park_given_parkingBoy_without_any_postion() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        for (int i = 0; i < parkingLot.getCapacity(); i++) {
            parkingLot.park(new Car());
        }
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        //when
        NoAvailablePostionException noAvailablePostionException = Assertions.assertThrows(NoAvailablePostionException.class, () -> parkingBoy.park(new Car()));
        //then
        Assertions.assertEquals("No Available Postion",noAvailablePostionException.getMessage());
    }

    @Test
    void should_return_parkIntoParkinglot1_when_park_given_parkingBoy_and_car_with_2parkinglot() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingLot parkingLot1 = new ParkingLot(2);
        Car car = new Car();
        //when
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot,parkingLot1);
        parkingBoy.park(car);
        //then
        Assertions.assertTrue(parkingLot.getCarlist().contains(car));
    }

    @Test
    void should_return_parkIntoParkinglot2_when_park_given_parkingBoy_and_car_with_2parkinglot_and_parkinglot1IsFull() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        for (int i=0;i<10;i++){
            parkingLot.park(new Car());
        }
        ParkingLot parkingLot1 = new ParkingLot(2);
        Car car = new Car();
        //when
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot,parkingLot1);
        parkingBoy.park(car);
        //then
        Assertions.assertTrue(parkingLot1.getCarlist().contains(car)&&!parkingLot.getCarlist().contains(car));
    }


    @Test
    void should_return_2parkingticket_when_fetch_given_parkingBoy_and_car_with_2parkinglot_and_bothParkedaCar() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingLot parkingLot1 = new ParkingLot(2);
        Car car1 = new Car();
        Car car2 = new Car();
        //when
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot,parkingLot1);
        ParkingTicket parkingTicket1 = parkingBoy.park(car1);
        ParkingTicket parkingTicket2 = parkingBoy.park(car2);
        //then
        Assertions.assertEquals(car1,parkingBoy.fetch(parkingTicket1));
        Assertions.assertEquals(car2,parkingBoy.fetch(parkingTicket2));
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_parkingBoy_with_2parkinglot_and_wrongticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingLot parkingLot1 = new ParkingLot(2);
        Car car1 = new Car();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot,parkingLot1);
        ParkingTicket parkingTicket = parkingBoy.park(car1);
        //when
        parkingBoy.fetch(parkingTicket);
        //then
        UnRecognizedParkingTicketException unRecognizedParkingTicketException = Assertions.assertThrows(UnRecognizedParkingTicketException.class, () -> parkingBoy.fetch(new ParkingTicket()));
        Assertions.assertEquals("Unrecognized ParkingTicket",unRecognizedParkingTicketException.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_parkingBoy_with_2parkinglot_and_usedticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingLot parkingLot1 = new ParkingLot(2);
        Car car1 = new Car();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot,parkingLot1);
        ParkingTicket parkingTicket = parkingBoy.park(car1);
        //when
        parkingBoy.fetch(parkingTicket);
        //then
        UnRecognizedParkingTicketException unRecognizedParkingTicketException = Assertions.assertThrows(UnRecognizedParkingTicketException.class, () -> parkingBoy.fetch(parkingTicket));
        Assertions.assertEquals("Unrecognized ParkingTicket",unRecognizedParkingTicketException.getMessage());
    }

    @Test
    void should_return_nothing_when_park_given_parkingBoy_with_2parkinglot_without_any_postion() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        for (int i = 0; i < parkingLot.getCapacity(); i++) {
            parkingLot.park(new Car());
        }
        ParkingLot parkingLot1 = new ParkingLot(2);
        for (int i = 0; i < parkingLot1.getCapacity(); i++) {
            parkingLot1.park(new Car());
        }
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot,parkingLot1);
        //when
        NoAvailablePostionException noAvailablePostionException = Assertions.assertThrows(NoAvailablePostionException.class, () -> parkingBoy.park(new Car()));
        //then
        Assertions.assertEquals("No Available Postion",noAvailablePostionException.getMessage());
    }
}
