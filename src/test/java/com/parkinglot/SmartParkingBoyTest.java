package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SmartParkingBoyTest {
    @Test
    void should_return_ticket_with_rightParkingLot_when_park_given_smartpParkingBoy_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);
        parkingLot.park(new Car());
        //when
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot,parkingLot2);
        ParkingTicket parkingTicket = smartParkingBoy.park(new Car());
        //then
        Assertions.assertEquals(2,parkingTicket.getNumber());
    }

    @Test
    void should_return_2car_when_fetch_given_smartParkingBoy_and_2ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        parkingLot.park(new Car());
        ParkingLot parkingLot2 = new ParkingLot(2);
        ParkingLot parkingLot3 = new ParkingLot(3);
        parkingLot3.park(new Car());
        Car car = new Car();
        Car car1 = new Car();
        //when
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot,parkingLot2,parkingLot3);
        ParkingTicket parkingTicket = smartParkingBoy.park(car);
        ParkingTicket parkingTicket1 = smartParkingBoy.park(car1);
        Car fetchedCar = smartParkingBoy.fetch(parkingTicket);
        Car fetchedCar1 = smartParkingBoy.fetch(parkingTicket1);
        //then
        Assertions.assertEquals(car,fetchedCar);
        Assertions.assertEquals(car1,fetchedCar1);
    }

    @Test
    void should_return_each_ticket_when_park_given_smartParkingBoy_and_two_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);
        Car car1 = new Car();
        Car car2 = new Car();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot,parkingLot2);
        ParkingTicket ticket1 = smartParkingBoy.park(car1);

        ParkingTicket ticket2 = smartParkingBoy.park(car2);
        //when
        //then
        Assertions.assertNotEquals(ticket1,ticket2);
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_smartParkingBoy_and_wrongticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car1 = new Car();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot);
        ParkingTicket parkingTicket = smartParkingBoy.park(car1);
        //when
        smartParkingBoy.fetch(parkingTicket);
        //then
        UnRecognizedParkingTicketException unRecognizedParkingTicketException = Assertions.assertThrows(UnRecognizedParkingTicketException.class, () -> smartParkingBoy.fetch(new ParkingTicket()));
        Assertions.assertEquals("Unrecognized ParkingTicket",unRecognizedParkingTicketException.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_smartParkingBoy_and_Usedticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        Car car1 = new Car();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot);
        ParkingTicket ticket1= smartParkingBoy.park(car1);
        //when
        smartParkingBoy.fetch(ticket1);
        //then
        UnRecognizedParkingTicketException unRecognizedParkingTicketException = Assertions.assertThrows(UnRecognizedParkingTicketException.class, () -> smartParkingBoy.fetch(ticket1));
        Assertions.assertEquals("Unrecognized ParkingTicket",unRecognizedParkingTicketException.getMessage());
    }

    @Test
    void should_return_nothing_when_park_given_smartParkingBoy_without_any_postion() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        for (int i = 0; i < parkingLot.getCapacity(); i++) {
            parkingLot.park(new Car());
        }
        ParkingLot parkingLot1 = new ParkingLot(2);
        for (int i = 0; i < parkingLot1.getCapacity(); i++) {
            parkingLot1.park(new Car());
        }
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot,parkingLot1);
        //when
        NoAvailablePostionException noAvailablePostionException = Assertions.assertThrows(NoAvailablePostionException.class, () -> smartParkingBoy.park(new Car()));
        //then
        Assertions.assertEquals("No Available Postion",noAvailablePostionException.getMessage());
    }
}
