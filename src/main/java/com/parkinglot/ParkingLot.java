package com.parkinglot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ParkingLot {
    private final Integer number;
    private final List<Car> carlist;
    public List<Car> getCarlist() {
        return carlist;
    }
    public Integer getCapacity() {
        return capacity;
    }

    private final Integer capacity=10;

    public ParkingLot(Integer number) {
        this.number = number;
        this.carlist = new ArrayList<>();
    }

    public ParkingTicket park(Car car) {
        if (carlist.size()>=capacity){
            throw new NoAvailablePostionException();
        }
        ParkingTicket parkingTicket = new ParkingTicket();
        car.setParkingTicket(parkingTicket);
        parkingTicket.setCar(car);
        parkingTicket.setNumber(this.number);
        this.carlist.add(car);
        return parkingTicket;
    }

    public Car fetch(ParkingTicket ticket) {
        if (ticket.getUsed().equals(true))throw  new UnRecognizedParkingTicketException();
        Car car1 = this.carlist.stream().filter(car -> car.getParkingTicket().equals(ticket)).findFirst().orElse(null);
        if (Objects.isNull(car1))throw  new UnRecognizedParkingTicketException();
        this.carlist.remove(car1);
        ticket.setUsed(true);
        return car1;
    }

    public Integer getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParkingLot that = (ParkingLot) o;
        return Objects.equals(number, that.number) && Objects.equals(carlist, that.carlist) && Objects.equals(capacity, that.capacity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, carlist, capacity);
    }
}
