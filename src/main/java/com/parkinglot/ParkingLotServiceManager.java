package com.parkinglot;


import java.util.Objects;

public class ParkingLotServiceManager {
    private final ParkingLot parkingLot;

    private final ParkingBoy[] parkingBoys;
    public ParkingLotServiceManager(ParkingLot parkingLot, ParkingBoy... parkingBoys) {
        this.parkingLot = parkingLot;
        this.parkingBoys = parkingBoys;
    }

    public ParkingTicket park(Car car) {
        return parkingLot.park(car);
    }

    public ParkingTicket parkByParkingBoy(Car car,ParkingBoy parkingBoy) {
        try {
            return parkingBoy.park(car);
        } catch (Exception e) {
            throw new ParkingBoyCantCompleteException();
        }
    }
    public Car fetch(ParkingTicket ticket) {
        if (!Objects.equals(ticket.getNumber(),parkingLot.getNumber()))throw new NotMyParkingLotException();
        return parkingLot.fetch(ticket);
    }

    public Car fetchByParkingBoy(ParkingTicket ticket,ParkingBoy pkb) {
        try {
            for (ParkingBoy parkingBoy: parkingBoys) {
                if (parkingBoy.equals(pkb)) return parkingBoy.fetch(ticket);
            }
        } catch (Exception e) {
            throw new ParkingBoyCantCompleteException();
        }
        throw new ParkingBoyCantCompleteException();
    }
}
