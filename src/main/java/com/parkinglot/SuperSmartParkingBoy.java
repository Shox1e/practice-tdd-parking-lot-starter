package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class SuperSmartParkingBoy {
    private final ParkingLot[] parkingLot;

    public SuperSmartParkingBoy(ParkingLot... parkingLots) {
        this.parkingLot = parkingLots;
    }

    public ParkingTicket park(Car car) {
        List<ParkingLot> parkingLotList = new ArrayList<>();
        for (ParkingLot lot : parkingLot) {
            if (lot.getCarlist().size() == 0){
                return lot.park(car);
            } else if (parkingLotList.isEmpty()) {
                parkingLotList.add(lot);
            }else if (countPostionRate(parkingLotList.get(0))<countPostionRate(lot)){
                parkingLotList.remove(0);
                parkingLotList.add(lot);
            }
        }
        if (parkingLotList.get(0).getCarlist().size()==parkingLotList.get(0).getCapacity()){
            throw new NoAvailablePostionException();
        }
        return parkingLotList.get(0).park(car);
    }

    private static Double countPostionRate(ParkingLot lot) {
        return (double) (lot.getCapacity() - lot.getCarlist().size()) / lot.getCapacity();
    }

    public Car fetch(ParkingTicket ticket) {
        for (ParkingLot lot : parkingLot) {
            if (lot.getNumber().equals(ticket.getNumber())){
                return lot.fetch(ticket);
            }
        }
        throw  new UnRecognizedParkingTicketException();
    }
}
