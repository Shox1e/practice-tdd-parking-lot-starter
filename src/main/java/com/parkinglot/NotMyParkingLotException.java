package com.parkinglot;

public class NotMyParkingLotException extends RuntimeException {
    public NotMyParkingLotException() {
        super("Not MyParkingLot ");
    }
}
