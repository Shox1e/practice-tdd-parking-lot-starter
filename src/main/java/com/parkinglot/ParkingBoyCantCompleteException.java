package com.parkinglot;

public class ParkingBoyCantCompleteException extends RuntimeException {
    public ParkingBoyCantCompleteException() {
        super("ParkingBoy Cant Complete");
    }
}
