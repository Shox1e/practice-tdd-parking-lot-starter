package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class SmartParkingBoy {
    private final ParkingLot[] parkingLot;

    public SmartParkingBoy(ParkingLot... parkingLots) {
        this.parkingLot = parkingLots;
    }

    public ParkingTicket park(Car car) {
        List<ParkingLot> parkingLotList = new ArrayList<>();
        for (ParkingLot lot : parkingLot) {
            if (lot.getCarlist().size() == 0){
                return lot.park(car);
            } else if (parkingLotList.isEmpty()) {
                parkingLotList.add(lot);
            }else if (parkingLotList.get(0).getCarlist().size()>lot.getCarlist().size()){
                parkingLotList.remove(0);
                parkingLotList.add(lot);
            }
        }
        if (parkingLotList.get(0).getCarlist().size()==parkingLotList.get(0).getCapacity()){
            throw new NoAvailablePostionException();
        }
        return parkingLotList.get(0).park(car);
    }

    public Car fetch(ParkingTicket ticket) {
        for (ParkingLot lot : parkingLot) {
            if (lot.getNumber().equals(ticket.getNumber())){
                return lot.fetch(ticket);
            }
        }
        throw  new UnRecognizedParkingTicketException();
    }
}
