package com.parkinglot;

public class NoAvailablePostionException extends RuntimeException {
    public NoAvailablePostionException() {
        super("No Available Postion");
    }
}
