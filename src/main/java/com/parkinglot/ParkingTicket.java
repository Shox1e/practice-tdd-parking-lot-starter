package com.parkinglot;

import java.util.Objects;

public class ParkingTicket {
    private Car car;

    public ParkingTicket() {
        this.isUsed = false;
    }

    private Boolean isUsed;

    private Integer parkingLotNumber;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Integer getNumber() {
        return parkingLotNumber;
    }

    public void setNumber(Integer parkingLotNumber) {
        this.parkingLotNumber = parkingLotNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParkingTicket that = (ParkingTicket) o;
        return Objects.equals(car, that.car) && Objects.equals(parkingLotNumber, that.parkingLotNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(car, parkingLotNumber);
    }

    public Boolean getUsed() {
        return isUsed;
    }

    public void setUsed(Boolean used) {
        isUsed = used;
    }
}
