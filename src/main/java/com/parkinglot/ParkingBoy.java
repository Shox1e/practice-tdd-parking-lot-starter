package com.parkinglot;


public class ParkingBoy {
    private final ParkingLot[] parkingLot;

    public ParkingBoy(ParkingLot... parkingLots) {
        this.parkingLot = parkingLots;
    }

    public ParkingTicket park(Car car) {
        for (ParkingLot lot : parkingLot) {
            if (lot.getCarlist().size()<lot.getCapacity()){
                return lot.park(car);
            }
        }
        throw new NoAvailablePostionException();
    }

    public Car fetch(ParkingTicket ticket) {
        for (ParkingLot lot : parkingLot) {
            if (lot.getNumber().equals(ticket.getNumber())){
                return lot.fetch(ticket);
            }
        }
        throw  new UnRecognizedParkingTicketException();
    }
}
